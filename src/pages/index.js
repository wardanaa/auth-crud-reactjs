import React, { Component } from 'react';
import Header from "../elements/header";
import Sidebar from "../elements/sidebar";
import {Link, Redirect} from 'react-router-dom';
import axios from 'axios';

export default class Index extends Component {
    state = {
        employee: [],
        toDashboard: false,
        isLoading: false
    };

    constructor(props) {
        super(props);
        this.url = 'https://api.smartbiz.id/api/demo/employee/page-search';
        this.urlDelete = 'https://api.smartbiz.id/api/demo/employee/';
        this.token1 = localStorage.getItem('token');
        this.token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImFhYTViODA2ZGE4YWNlNDM0ZWUzMTM1M2QyNjlhZjkwYTlhZTViN2M2YTU5MWExYWE0NmFiM2ZlZGQxYjA4YjE2NTZmMWJiMzUxMGEzNjM2In0.eyJhdWQiOiIyIiwianRpIjoiYWFhNWI4MDZkYThhY2U0MzRlZTMxMzUzZDI2OWFmOTBhOWFlNWI3YzZhNTkxYTFhYTQ2YWIzZmVkZDFiMDhiMTY1NmYxYmIzNTEwYTM2MzYiLCJpYXQiOjE2MDM1NjMxMjksIm5iZiI6MTYwMzU2MzEyOSwiZXhwIjoxNjAzNTY2NzI5LCJzdWIiOiIxMSIsInNjb3BlcyI6WyIqIl19.fZ22CU2H0SsldyRViFd7qBSGwUu3ypZ91zJNu62UcIGEkJxMCeSFtOSK2BKyQDV9H7xGvPdJ5MWt47Nwaxd9WkGnAKx31UeFnbHrCuOiTix8vXrUAVGo8PxwbvY8C2T5ONtAl6iUbV1udGKGv7kQKwolYei7t1xJ4wcKI53jcqWRijzCrPvbIxzhpQuivpC-i2BkYWRE-sATFxOpTxGGaPaCSOW3qbwk1hmOGcm5dw2-tTKogswin3cbl1c_eZR_Lnbc6yvOPV0Vu0bVtjc6SEIDD4_C7Yu4xDHX-YhyvfM2dTtzTrL2ny9-Qtk__clgxmzka5hfCOe2dxcYLuC-ERxlaeOxYvwXfwk0D-gbla2Lfj_0Xhh-AZgtMke3BWcMz5oISdiJ9bH40UEuQfzOVG3lwlm2wmkHZKa36u1zt8pUNwkfcAjFOxEtgD5edKgOvuUWOxI1lCa-_4IqkOIgcswRd3hcXhMQ2oBOrOnINx9TpPk0fG6Sf3xASUtfCvpaMn6xiituueZE2RVVY9VoLEFUd3N1JlzCtFCQSTPiL6hJ3is4bZkv92qs0rarUw6V7_L7uisYulOM7vxEmtOxQ6F72Zdz_JsJI3SW0MPL6yo1X_Dstk_S084OMgh5NoneYS7JDiHwk0H7xgeE6jfcSNGWfoIE-tOS0yCLb1i5vzU';
        
    }

    componentDidMount() {
        const bodyFormData = {
            "query": {
                "value": "string"
            },
            "start_birth_date": "",
            "end_birth_date": "",    
            "pagination": {
                "page": 1,
                "perpage": 10
            },
            "sort": {
                "sort": "ASC",
                "field": "id"
            }
        }
        axios.post(this.url, bodyFormData, { headers: { 'Authorization': 'Bearer ' + this.token}})
            .then(response => {
                const employee = response.data.rows;
                this.setState({ employee });
            })
            .catch(error => {
                this.setState({ toDashboard: true });
                console.log(error);
            });
    }

    handleClickDelete = event => {
        axios.delete(this.urlDelete + event.target.value , { headers: { 'Authorization': 'Bearer ' + this.token}})
            .then(response => {
                this.componentDidMount();
                this.setState({ isLoading: true})
            })
            .catch( error => {
                console.log(error.toString());
                this.setState({ toDashboard: true });
            });
    };

    render() {
        if (this.state.toDashboard === true) {
            return <Redirect to='/' />
        }
        return (
            <div>
                <Header/>
                <div id="wrapper">
                    <Sidebar/>
                    <div id="content-wrapper">
                        <div className="container-fluid">
                            <ol className="breadcrumb">
                                <li className="breadcrumb-item">
                                    <Link to={'/dashboard'} >Dashboard</Link>
                                </li>
                                <li className="breadcrumb-item active">CRUD App</li>
                                <li className="ml-auto"><Link to={'add'}>Add Employee</Link></li>
                            </ol>
                            <div className="card mb-3">
                                <div className="card-header"><i className="fas fa-table"></i>
                                    &nbsp;&nbsp;Employee List
                                </div>
                                <div className="card-body">
                                    <table className="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>NIP</th>
                                            <th>Full Name</th>
                                            <th>Nick Name</th>
                                            <th>Birth Date</th>
                                            <th>Address</th>
                                            <th>Phone</th>
                                            <th>Mobile</th>
                                            <th>Email</th>
                                            <th className="text-center">Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            {this.state.employee.map((employee , index)=>
                                                <tr key={employee.id}>
                                                    <td>{index + 1}</td>
                                                    <td>{employee.nip}</td>
                                                    <td>{employee.full_name}</td>
                                                    <td>{employee.nick_name}</td>
                                                    <td>{employee.birth_date}</td>
                                                    <td>{employee.address}</td>
                                                    <td>{employee.phone}</td>
                                                    <td>{employee.mobile}</td>
                                                    <td>{employee.email}</td>
                                                    <td className="text-center">
                                                        <Link className="btn btn-sm btn-info" to={{ pathname: 'edit', search: '?id=' + employee.id }}>Edit</Link>
                                                        &nbsp; | &nbsp;
                                                        <button value={employee.id} className="btn btn-sm btn-danger" disabled={ index === 0  ? true : false} onClick={this.handleClickDelete} >Delete</button>
                                                    </td>
                                                </tr>)
                                            }
                                        </tbody>
                                    </table>
                                </div>
                                <div className="card-footer small text-muted">Updated yesterday at 11:59 PM</div>
                            </div>
                        </div>
                        <footer className="sticky-footer">
                            <div className="container my-auto">
                                <div className="copyright text-center my-auto">
                                    <span>Copyright © Your Website 2019</span>
                                </div>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        );
    }
}
