import React, {Component} from 'react';
import Header from "../elements/header";
import Sidebar from "../elements/sidebar";
import {Link, Redirect} from "react-router-dom";
import axios from 'axios';

export default class AddPage extends Component {

    state = {
        redirect: false,
        toDashboard: false,
        isLoading: false
    };

    handleSubmit = event => {
        event.preventDefault();
        this.setState({isLoading: true});
        const token1 = localStorage.getItem('token');
        const token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImFhYTViODA2ZGE4YWNlNDM0ZWUzMTM1M2QyNjlhZjkwYTlhZTViN2M2YTU5MWExYWE0NmFiM2ZlZGQxYjA4YjE2NTZmMWJiMzUxMGEzNjM2In0.eyJhdWQiOiIyIiwianRpIjoiYWFhNWI4MDZkYThhY2U0MzRlZTMxMzUzZDI2OWFmOTBhOWFlNWI3YzZhNTkxYTFhYTQ2YWIzZmVkZDFiMDhiMTY1NmYxYmIzNTEwYTM2MzYiLCJpYXQiOjE2MDM1NjMxMjksIm5iZiI6MTYwMzU2MzEyOSwiZXhwIjoxNjAzNTY2NzI5LCJzdWIiOiIxMSIsInNjb3BlcyI6WyIqIl19.fZ22CU2H0SsldyRViFd7qBSGwUu3ypZ91zJNu62UcIGEkJxMCeSFtOSK2BKyQDV9H7xGvPdJ5MWt47Nwaxd9WkGnAKx31UeFnbHrCuOiTix8vXrUAVGo8PxwbvY8C2T5ONtAl6iUbV1udGKGv7kQKwolYei7t1xJ4wcKI53jcqWRijzCrPvbIxzhpQuivpC-i2BkYWRE-sATFxOpTxGGaPaCSOW3qbwk1hmOGcm5dw2-tTKogswin3cbl1c_eZR_Lnbc6yvOPV0Vu0bVtjc6SEIDD4_C7Yu4xDHX-YhyvfM2dTtzTrL2ny9-Qtk__clgxmzka5hfCOe2dxcYLuC-ERxlaeOxYvwXfwk0D-gbla2Lfj_0Xhh-AZgtMke3BWcMz5oISdiJ9bH40UEuQfzOVG3lwlm2wmkHZKa36u1zt8pUNwkfcAjFOxEtgD5edKgOvuUWOxI1lCa-_4IqkOIgcswRd3hcXhMQ2oBOrOnINx9TpPk0fG6Sf3xASUtfCvpaMn6xiituueZE2RVVY9VoLEFUd3N1JlzCtFCQSTPiL6hJ3is4bZkv92qs0rarUw6V7_L7uisYulOM7vxEmtOxQ6F72Zdz_JsJI3SW0MPL6yo1X_Dstk_S084OMgh5NoneYS7JDiHwk0H7xgeE6jfcSNGWfoIE-tOS0yCLb1i5vzU';
        const url1 = 'https://gowtham-rest-api-crud.herokuapp.com/employees';
        const url = 'https://api.smartbiz.id/api/demo/employee';
        const nip = document.getElementById('inputNIP').value;
        const full_name = document.getElementById('inputFullName').value;
        const nick_name = document.getElementById('inputNickName').value;
        const birth_date = document.getElementById('inputBirthDate').value;
        const address = document.getElementById('inputAddress').value;
        const phone = document.getElementById('inputPhone').value;
        const mobile = document.getElementById('inputMobile').value;
        const email = document.getElementById('inputEmail').value;

        let bodyFormData = new FormData();
        bodyFormData.set('nip', nip);
        bodyFormData.set('full_name', full_name);
        bodyFormData.set('nick_name', nick_name);
        bodyFormData.set('birth_date', birth_date);
        bodyFormData.set('address', address);
        bodyFormData.set('phone', phone);
        bodyFormData.set('mobile', mobile);
        bodyFormData.set('email', email);
        axios.post(url, bodyFormData, { headers: { 'Authorization': 'Bearer ' + token}})
            .then(result => {
                if (result.data.status) {
                    this.setState({redirect: true, isLoading: false})
                }
            })
            .catch(error => {
                this.setState({ toDashboard: true });
                console.log(error);
            });
    };

    renderRedirect = () => {
        if (this.state.redirect) {
            return <Redirect to='/dashboard' />
        }
    };

    render() {
        const isLoading = this.state.isLoading;
        if (this.state.toDashboard === true) {
            return <Redirect to='/' />
        }
        return (
            <div>
                <Header/>
                <div id="wrapper">
                    <Sidebar></Sidebar>
                    <div id="content-wrapper">
                        <div className="container-fluid">
                            <ol className="breadcrumb">
                                <li className="breadcrumb-item">
                                    <Link to={'/dashboard'} >Dashboard</Link>
                                </li>
                                <li className="breadcrumb-item active">Add</li>
                            </ol>
                        </div>
                        <div className="container-fluid">
                            <div className="card mx-auto">
                                <div className="card-header">Employee Add</div>
                                <div className="card-body">
                                    <form onSubmit={this.handleSubmit}>
                                        <div className="form-group">
                                            <div className="form-row">
                                                <div className="col-md-6">
                                                    <div className="form-label-group">
                                                        <input type="text" id="inputNIP" className="form-control" placeholder="Enter NIP" required="required" autoFocus="autofocus" />
                                                        <label htmlFor="inputNIP">NIP</label>
                                                    </div>
                                                </div>
                                                <div className="col-md-6">
                                                    <div className="form-label-group">
                                                        <input type="text" id="inputFullName" className="form-control" placeholder="Enter full name..." required="required" />
                                                        <label htmlFor="inputFullName">Full Name</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <div className="form-row">
                                                <div className="col-md-6">
                                                    <div className="form-label-group">
                                                        <input type="text" id="inputNickName" className="form-control" placeholder="Enter nick name..." required="required" />
                                                        <label htmlFor="inputNickName">Nick Name</label>
                                                    </div>
                                                </div>
                                                <div className="col-md-6">
                                                    <div className="form-label-group">
                                                        <input type="datetime-local" id="inputBirthDate" className="form-control" placeholder="Enter birth date..." required="required" min="1987-04-04T00:00" max="1987-04-05T00:00" />
                                                        <label htmlFor="inputBirthDate">Birth Date</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <div className="form-row">
                                                <div className="col-md-6">
                                                    <div className="form-label-group">
                                                        <input type="text" id="inputAddress" className="form-control" placeholder="Enter address..." required="required" />
                                                        <label htmlFor="inputAddress">Address</label>
                                                    </div>
                                                </div>
                                                <div className="col-md-6">
                                                    <div className="form-label-group">
                                                        <input type="text" id="inputPhone" className="form-control" placeholder="Enter phone number..." required="required"/>
                                                        <label htmlFor="inputPhone">Phone</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="form-group">
                                            <div className="form-row">
                                                <div className="col-md-6">
                                                    <div className="form-label-group">
                                                        <input type="number" id="inputMobile" className="form-control" placeholder="Enter mobile number..." required="required" />
                                                        <label htmlFor="inputMobile">Mobile</label>
                                                    </div>
                                                </div>
                                                <div className="col-md-6">
                                                    <div className="form-label-group">
                                                        <input type="email" id="inputEmail" className="form-control" placeholder="Enter email address..." required="required" />
                                                        <label htmlFor="inputEmail">Email</label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <button className="btn btn-primary btn-block" type="submit" disabled={this.state.isLoading ? true : false}>Add Employee &nbsp;&nbsp;&nbsp;
                                            {isLoading ? (
                                                <span className="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                                             ) : (
                                                 <span></span>
                                             )}
                                        </button>
                                    </form>
                                    {this.renderRedirect()}
                                </div>
                            </div>
                        </div>

                        <footer className="sticky-footer">
                            <div className="container my-auto">
                                <div className="copyright text-center my-auto">
                                    <span>Copyright © Your Website <div>{(new Date().getFullYear())}</div></span>
                                </div>
                            </div>
                        </footer>
                    </div>
                </div>
            </div>
        );
    }
}
